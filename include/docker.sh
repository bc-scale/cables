#!/bin/bash -au

function is_bot_stable () {
    docker ps --filter status=running | grep -q cables-bot
    return $?
}

function check_bot () {
    echo '>>> Waiting for bot stability assessment (5s)'
    sleep 5
    if ! is_bot_stable; then
        echo '❌ Bot is unstable!' >&2
    else
        echo '✅ Bot is stable!'
    fi
}

function docker_clean () {
    if [[ "$CMD" =~ up|(re)?start ]]; then
        test ! is_bot_stable && return
    fi

    echo '>>> Cleaning up dangling docker containers and images'
    docker container prune -f
    images=("$(docker images -f "dangling=true" -q)")
    if [ -n "${images[@]}" ]; then
        docker rmi "${images[@]}"
    fi
}

function build_all () {
    echo '>>> Building images'
    docker-compose ${BASE_ARGS[*]} -f infra/docker-compose.base.yml build
    docker-compose ${APP_ARGS[*]} -f "$APP_COMPOSE_FILE" build
}

function compose_cmd () {
    if ! docker-compose ${APP_ARGS[*]} -f "$APP_COMPOSE_FILE" $@; then
        return
    fi
}
