from discord import (Forbidden, NotFound, HTTPException)

refs = [
    {"m": 0, "c": 0}
]

msgs = []
for o in refs:
    c = ctx.guild.get_channel(int(o["c"]))
    m = await c.fetch_message(int(o["m"]))
    msgs += [m]

f = []
for m in msgs:
    try:
        await m.delete(reason="Requested by mentioned user")
    except (Forbidden, NotFound, HTTPException):
        f += [m]

s = len(refs) - len(f)
text = f"Deleted **{s}** / {len(refs)} messages successfully\n"
text += "*Failures:*" + ", ".join([m.id for m in f]) if f else ""
await ctx.channel.send(text)
