ALTER TABLE guild_invite_codes
    ADD COLUMN IF NOT EXISTS revoked_at timestamp NULL;