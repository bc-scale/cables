CREATE UNIQUE INDEX IF NOT EXISTS idx_rrr_msg_role
    ON role_react_reactions (msg_ref, role);

ALTER TABLE role_react_reactions
    ADD CONSTRAINT fk_rrr_rrm_id FOREIGN KEY (msg_ref) REFERENCES role_react_messages (id) ON DELETE CASCADE;