from typing import Optional
from discord import Guild, Member, Permissions


def is_channel_readable(perms: Permissions) -> bool:
    return (
        perms.view_channel
        and (perms.read_messages and perms.read_message_history)
    )


def is_channel_writable(perms: Permissions) -> bool:
    return (
        perms.add_reactions
        and (perms.send_messages or perms.send_messages_in_threads)
    )


def writable_channels_by_member(guild: Guild, member: Member) -> Optional[list]:
    matches, threads = list(), list()
    channels = guild.text_channels + guild.voice_channels
    for channel in channels:
        perms = channel.permissions_for(member)
        if not (is_channel_readable(perms) and is_channel_writable(perms)):
            continue
        matches.append(channel.id)
        if getattr(channel, "threads", None) and channel.threads:
            threads += channel.threads
    for thread in threads:
        matches.append(thread.id)

    return matches
