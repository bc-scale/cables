from __future__ import annotations
from typing import TYPE_CHECKING
from datetime import datetime, timezone
from discord import Member

from ..bot.types import StateData
if TYPE_CHECKING:
    from ..bot import CablesBot as Bot


def get_first_join(bot: Bot, member: Member) -> StateData:
    q = """
        SELECT joined_at FROM guild_member_joins
        WHERE guild = %s AND "user" = %s
        ORDER BY id ASC
        LIMIT 1
    """
    v = (member.guild.id, member.id)
    with bot.db.connection() as conn:
        cur = conn.execute(q, v)
        if cur.rowcount:
            result = cur.fetchone()
            first_join: datetime = result.joined_at
            first_join = first_join.replace(tzinfo=timezone.utc)
            is_rejoin = (True if first_join < member.joined_at else False)
            return StateData(state=is_rejoin, data=first_join)
        return None
