# Kudos to dico-api/dico and Pycord-Development/pycord for the examples
# https://github.com/dico-api/dico/blob/master/dico/voice/encryptor.py
# https://github.com/Pycord-Development/pycord/blob/2f0e1764a84333bf6eec1c94aa7207fd990556dc/discord/voice_client.py#L555

from typing import Union
import nacl.secret
import nacl.utils


class CryptographyController:
    def __init__(self, secret_key: str) -> None:
        self.secret_key = secret_key
        self.box = nacl.secret.SecretBox(self.secret_key)

    def encrypt(self, data: bytes) -> bytes:
        if type(data) is not bytes:
            data = self._coerce('data', data)

        nonce = nacl.utils.random(nacl.secret.SecretBox.NONCE_SIZE)
        return self.box.encrypt(bytes(data), nonce).ciphertext + nonce

    def decrypt(self, data: bytes) -> bytes:
        if type(data) is not bytes:
            raise ValueError("data must be of type bytes")

        nonce_size = nacl.secret.SecretBox.NONCE_SIZE
        nonce = data[-nonce_size:]
        return self.box.decrypt(bytes(data[:-nonce_size]), nonce)

    def _coerce(self, name: str, value: Union[bytes, bytearray]):
        try:
            if type(value) is str:
                return bytearray(value, "UTF-8")
            else:
                return bytearray(value)
        except (SyntaxError, TypeError, ValueError):
            raise ValueError(
                f"{name} must be one of types: "
                + "[bytearray, int, list, tuple, str]"
            )
