from __future__ import annotations
from typing import TYPE_CHECKING

import logging

from typing import (Union, Optional)
from discord import (
    Guild, Member, TextChannel, Message,
    Embed, EmbedField, Interaction, WebhookMessage,
)

from discord.utils import utcnow
from datetime import (datetime, timedelta)
from .permissions import is_channel_readable
from .channels import filter_guild_channels
from ..bot import constants
from ..bot.config import GuildConfig
from ..bot.errors import report_guild_error
from ..util import (calc, messages)

if TYPE_CHECKING:
    from ..bot import (
        CablesBot as Bot,
        CablesAppContext as ApplicationContext,
    )

logger = logging.getLogger(__name__)
mdash = constants.Symbols.mdash


## JOB HELPERS

def _parse_message(
    msg: Message, history: dict,
    counts: tuple[int, int, int],
) -> tuple[dict, int, int, int]:
    category = msg.channel.category.name
    channel = msg.channel.name
    (msgs, media, links) = counts

    has_media = messages.has_media(msg)
    has_link = messages.has_text_link(msg)

    if msg.author.id not in history[category][channel]["participants"]:
        history[category][channel]["participants"] += [msg.author.id]
    history[category][channel]["events"] += 1
    msgs += 1

    if has_link:
        history[category][channel]["links"] += 1
        links += 1
    if has_media:
        history[category][channel]["media"] += 1
        media += 1

    return (history, msgs, media, links)


async def _scan_history(
    channel: TextChannel,
    daterange: tuple[datetime, datetime],
    history: dict,
    counts: tuple[int, int, int]
) -> tuple[dict, int, int, int]:
    (after, before) = daterange
    (msgs, media, links) = counts

    async for msg in channel.history(
        after=after, before=before,
        limit=None, oldest_first=False
    ):
        if (
            msg.type.name not in ("default", "reply")
            or msg.author.bot
            or msg.webhook_id
            or msg.flags.is_crossposted
        ):
            continue

        # update total and channel-specific counts between iterations
        tup = _parse_message(msg, history, (msgs, media, links))
        (history, msgs, media, links) = tup

    return (history, msgs, media, links)


async def _scan_channels(
    channels: list[TextChannel],
    after: datetime, before: datetime
) -> tuple[int, list[int], int, int, int]:
    history = dict()
    (chans, achans, parts,
     msgs, media, links) = 0, 0, [], 0, 0, 0

    for c in channels:
        chans += 1
        try:
            assert history[c.category.name]
        except KeyError:
            history[c.category.name] = {}
        history[c.category.name][c.name] = dict(
            id=c.id,
            events=0,
            participants=[],
            media=0, links=0,
        )

        (history, msgs, media, links) = await _scan_history(
            c, (after, before), history, (msgs, media, links)
        )
        if history[c.category.name][c.name]["events"]:
            achans += 1
        if history[c.category.name][c.name]["participants"]:
            parts += history[c.category.name][c.name]["participants"]

    parts = len(list(dict.fromkeys(parts)))  # count deduplicated parts

    return (achans, parts,
            msgs, media, links)


def get_targets(bot: Bot, guild: Guild) -> Optional[list[TextChannel]]:
    gid = guild.id

    chids = bot.get_guild_key(gid, "statistics", "summary_targets")
    if (channels := filter_guild_channels(guild, chids)):
        return channels

    channels = []
    for c in guild.text_channels:
        if is_channel_readable(c.permissions_for(guild.me)):
            channels += [c]
    return channels


def _format_embed_fields(
    days: int, counts: tuple[int, int, int]
) -> list[EmbedField]:
    (msgs, media, links) = counts

    totals = f"💬 `{msgs}` posts\n"
    totals += f"🖼 `{media}` media\n" if media else ""
    totals += f"🔗 `{links}` links" if links else ""
    fields = [EmbedField(name="Totals", value=totals, inline=True)]

    if days > 1:
        daily_msgs = calc.rounded_division(msgs, days)
        daily_media = calc.rounded_division(media, days)
        daily_links = calc.rounded_division(links, days)
        daily = f"💬 `{daily_msgs}` posts\n"
        daily += f"🖼 `{daily_media}` media\n" if daily_media else ""
        daily += f"🔗 `{daily_links}` links" if daily_links else ""
        fields += [EmbedField(name="Daily Avg.", value=daily, inline=True)]

    return fields


async def _report_perf(
    bot: Bot,
    guild: Guild,
    target: Union[Message, WebhookMessage],
    config: int,
    timings: tuple[int, int],
) -> None:
    (runtime, threshhold) = timings
    fields = [
        EmbedField(name="Task", value="Message aggregation", inline=True),
        EmbedField(name="Period", value=f"{config} days", inline=True),
        EmbedField(name="Runtime", value=f"{runtime} sec", inline=True),
        EmbedField(name="Threshold", value=f"{threshhold} sec", inline=True),
        EmbedField(name="Guild", value=guild.id, inline=True),
        EmbedField(name="Event", value=f"[Jump]({target.jump_url})", inline=True),
    ]
    embed = Embed(
        color=constants.Colours.dark_goldenrod,
        url=target.jump_url,
        fields=fields,
    )
    embed.set_author(
        name="Performance report",
        icon_url=constants.Icons.warning,
    )
    bot.loop.create_task(
        bot.debug_channel.send(embeds=[embed])
    )


## JOB RUNNER

async def job(
    channels: list[TextChannel],
    target: Union[Interaction, Message],
    days: Optional[int] = 7,
) -> Union[Message, WebhookMessage]:
    now = utcnow()
    after = now - timedelta(days + 1)

    (active_chans, parts,
     msgs, media, links) = await _scan_channels(channels, after, now)

    label = "day" if days == 1 else "days"
    fields = _format_embed_fields(days, (msgs, media, links))
    embed = Embed(
        title="🧮 Server Activity",
        description=(f"Over the last **{days}** {label}, we saw "
                     + f"**{parts}** participants between "
                     + f"**{active_chans}** channels."),
        fields=fields,
    )

    if isinstance(target, Message):
        if target.embeds and target.embeds[0] == embed:
            return
        await target.edit("", embed=embed)
        return target
    elif isinstance(target, Interaction):
        return await target.followup.send(embeds=[embed])


async def job_wrapper(
    bot: Bot,
    target: Union[Interaction, Message],
    days: Optional[int] = 7,
) -> None:
    guild = target.guild
    channels = get_targets(bot, guild)

    start = utcnow()
    j = {"job": "message_aggregation",
         "guild": guild.id,
         "days": days}
    log = {"LARGE_JOB_START": j}
    logger.info(log)

    msg = await job(channels, target, days=days)

    timing = (utcnow() - start).seconds
    j["runtime_secs"] = timing
    log = {"LARGE_JOB_END": j}
    logger.info(log)

    threshhold = 90
    if timing > threshhold:
        try:
            await _report_perf(bot, guild, msg, days, (timing, threshhold))
        except:  # noqa: E722
            pass


## SETUP HELPERS

async def perms_check(
    ctx: ApplicationContext,
    guild: Guild, channel: TextChannel,
) -> None:
    perms = channel.permissions_for(guild.me)
    if not (
        perms.view_channel
        or perms.read_messages
        or perms.read_message_history
        or perms.send_messages
    ):
        report_text = (
            f"❌ Lacking permissions for {channel.mention}! "
            + "Grant view, read message history and send messages."
        )
        await report_guild_error(
            ctx.bot, ctx,
            None, guild, report_text,
        )
        raise ValueError


async def configure_guild(
    bot: Bot,
    config: GuildConfig,
    channel: TextChannel,
) -> tuple[Optional[Message], bool]:
    guild = channel.guild
    updates = False

    enabled = config.get("feature_flags", "statistics")
    if not enabled:
        config.set("feature_flags", "statistics", "on")
        updates = True

    chid = channel.id
    chset = config.get("statistics", "summary_channel")
    if not chset or chset != chid:
        config.set("statistics", "summary_channel", str(chid))
        updates = True

    create = None
    mid = config.get("statistics", "summary_msg")
    if chset != chid:
        create = True
    else:
        msg = await messages.get_message(bot, guild, chid, mid)
        if not msg:
            create = True

    if create:
        msg = await messages.create_post(bot, guild, chid)
    if mid != msg.id:
        config.set("statistics", "summary_msg", str(msg.id))

    return (msg, bool(updates or create))
