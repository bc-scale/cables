from __future__ import annotations
from typing import TYPE_CHECKING

import asyncio
import logging
import discord.utils
from cabling_bot.bot.constants import DevGuildTemplate
from cabling_bot.util.discord import get_invitation
from cabling_bot.util.user import can_send_dm

from typing import (Optional, Union, Awaitable)
from discord import (
    Guild, NotFound, Role, Member, Permissions, TextChannel, User,
    HTTPException, Forbidden
)

if TYPE_CHECKING:
    from cabling_bot.bot import CablesBot as Bot

logger = logging.getLogger(__name__)


async def init(bot: Bot, op: User) -> Optional[Guild]:
    guild_name = bot.get_config(
        "privileged.dev_guild_name",
        "cables development"
    )
    guild = _find_guild(bot, guild_name)
    if (
        not guild
        and bot.get_config("privileged.create_dev_guild", False)
    ):
        if len(bot.guilds) > 10:
            logger.error("Cannot create guild; must be in <10 guilds")
            return False
        guild = await _create_guild(bot, guild_name)

    bot.debug_channel = _get_debug_channel(bot, guild)

    has_oper, op = await _has_oper(bot, guild)
    if (
        guild and not has_oper
        and (op and type(op) is User)
    ):
        await _invite_oper(bot, guild, op)

    return guild


def _find_guild(bot: Bot, guild_name: str) -> Optional[Guild]:
    guild = (
        bot.get_guild(
            bot.get_config(
                "privileged.dev_guild_id",
                None
            )
        )
        # or discord.utils.find(
        #     lambda g: g.name == guild_name,
        #     bot.guilds
        # )
    )
    if not guild:
        logger.info("Dev guild doesn't yet exist")
    return guild


async def _create_guild(bot: Bot, name: str) -> Union[Guild, bool]:
    """Attempts to create the dev guild

    Tries to do so from a server template, with \
        `cabling_bot.bot.constants.DevGuildTemplate.current`

    Falls back to creating an empty guild

    Returns either `Guild` or `None`"""
    try:
        template = await bot.fetch_template(DevGuildTemplate.current)
    except (HTTPException, Forbidden) as e:
        logger.error(f"Failed to retrieve dev guild template: {e}")
        template = None

    guild = None
    if not template:
        try:
            guild = await bot.create_guild(name=name)
        except HTTPException as e:
            err = e
    else:
        try:
            guild = await template.create_guild(name=name)
        except HTTPException as e:
            err = e

    if not guild:
        logger.error(f"Failed to create guild: {err}")
    else:
        logger.info(f"Created dev guild! ({guild.id})")
    return guild


def _get_debug_channel(bot: Bot, guild: Guild) -> Optional[TextChannel]:
    if bot.environment == "production":
        name = "prod-logs"
    elif bot.environment == "development":
        name = "dev-logs"
    return (
        guild.get_channel(
            bot.get_config("privileged.debug_channel", None)
        ) or discord.utils.find(
            lambda c: c.name == name,
            guild.text_channels
        )
    )


async def _has_oper(
    bot: Bot, guild: Guild,
    retry: Optional[int] = None
) -> tuple[bool, Optional[User]]:
    """Checks for the primary bot operator and whether or not they're \
        a member on the given guild.

    Returns a `tuple` with the outcome as a `boolean`, and one of:
    - `None` when all checks fail; probably indicates API outage
    - `User` when the operator *isn't* a guild member
    - `Member` when the operator *is* a guild member
    """
    op = await _get_oper(bot)
    if not op or type(op) is int:
        return await _retry(
            _has_oper,
            bot, guild, (retry or 1), 3
        )

    try:
        op = await guild.fetch_member(op.id)
    except NotFound:
        return (False, op)
    except HTTPException:
        return await _retry(
            _has_oper,
            bot, guild, (retry or 1), 3
        )
    return (True, op)


async def _get_oper(bot: Bot) -> Optional[Union[User, int]]:
    """Try to retrieve an object representing the primary bot operator.

    - Checks the `privileged.bot_operator` config key
    - Returns cached operator from prior Discord app info API endpoint call

    Returns a `User`, the ID as an `int`, or `None`"""
    op_id = bot.get_config("privileged.bot_operator", None)
    if op_id == bot.operator.id:
        return bot.operator
    try:
        return await bot.fetch_user(op_id)
    except (HTTPException, NotFound, KeyError):
        return (bot.operator or op_id)


async def _invite_oper(bot: Bot, guild: Guild, op: User) -> bool:
    if not await can_send_dm(op, bot.user):
        logger.error(f"User ({op}) failed messagable check")
        return None
    invite = await get_invitation(bot, guild, print_err=True)
    if not invite:
        return None
    try:
        await op.send(invite.url)
        text = (f"Invited operator ({op}) to guild via invite {invite.code}")
        logger.info(text)
        return True
    except (HTTPException, Forbidden) as e:
        logger.error(f"Failed to DM invite to operator: {e}")
        return False


async def _retry(
    func: Awaitable,
    *args,
    retry: Optional[int] = None,
    max: Optional[int] = 3
):
    bot: Bot
    bot, _ = args

    logger.error("Failed to detect bot operator")
    if bot.environment != "production":
        appinfo = await bot.application_info()
        op_config = bot.get_config("privileged.bot_operator", None)
        op_app = appinfo.owner
        logger.debug(f"privileged.bot_operator={op_config}")
        logger.debug(f"Bot.application_info().owner={op_app}")

    if not retry or retry < (max + 1):
        if not retry:
            retry = 1
        else:
            retry += 1
        await asyncio.sleep(2.0)
        return await ()(func)(*args, retry)


async def handle_join(bot: Bot, member: Member) -> None:
    guild = member.guild
    if guild.id != bot.dev_guild.id:
        return
    if member.id not in [
        bot.get_config("privileged.bot_operator"),
        bot.operator,
        bot.owner_id
    ]:
        return
    if discord.utils.find(lambda r: r.name == "👑", member.roles):
        return

    admin_role = discord.utils.find(lambda r: r.name == "👑", guild.roles)
    if not admin_role:
        admin_role = await _create_admin_role(guild)
    if not admin_role:
        return
    try:
        await member.add_roles(admin_role)
    except HTTPException as e:
        logger.error(f"Failed to grant admin role to operator: {e}")
        return
    logger.info("Granted administrator rights to operator on dev guild")


async def _create_admin_role(guild: Guild) -> Optional[Role]:
    try:
        role: Role = await guild.create_role(
            name="👑",
            permissions=Permissions.all()
        )
        role_position = (guild.me.top_role.position + 1)
        await role.edit(position=role_position)
    except (AttributeError, Forbidden) as e:
        logger.error(f"Failed to set up admin role on dev guild: {e}")
        return None
    logger.info("Created administrator role on dev guild")
    return role
