import logging
import requests
from typing import Optional
from urllib.parse import urlparse

logger = logging.getLogger(__name__)

USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36"


class EHentaiService:
    def _is_gallery(self, url: str) -> Optional[tuple]:
        path = urlparse(url).path.split("/")
        _, route, gallery_id, gallery_token, *_ = path
        is_gallery = route == "g"
        if not is_gallery:
            # logger.info(f"path component '{route}' does not contain match 'g'")
            return
        return (gallery_id, gallery_token)

    def get(self, url: str) -> Optional[dict]:
        try:
            gallery_id, gallery_token = self._is_gallery(url)
        except TypeError:
            return False

        payload = {
            "method": "gdata",
            "gidlist": [[gallery_id, gallery_token]],
            "namespace": 1,
        }
        headers = {
            "Content-Type": "application/json",
            "Accept": "application/json;q=0.9,*/*",
            "Accept-Language": "en-US;q=0.9,en;q=0.8",
            "User-Agent": USER_AGENT,
        }
        # TODO: add retry handling
        response = requests.post(
            "https://api.e-hentai.org/api.php",
            json=payload,
            headers=headers,
        )
        response_json = response.json()
        # logger.info(response_json)

        err = None
        try:
            err = response_json["gmetadata"][0]["error"]
        except KeyError:
            pass
        if err:
            logger.warning(f"e-hentai API error: {err}")
            return False

        # logger.info(metadata)
        return response_json["gmetadata"][0]
