import logging
from discord import (Guild, Message)
from discord.ext import commands
from cabling_bot.bot import (
    CablesCog as Cog,
    CablesBot as Bot,
    CablesContext as Context
)

logger = logging.getLogger(__name__)


class SayHello(Cog):
    """Hello, world! See `help hewwo` for more info."""
    def __init__(self, bot: Bot):
        self._bot = bot
        bot.config_keys.new(
            "fun", "greeting",
            "(str) A response to override the bot's greeting triggers with.",
            None
        )

    def _say_hello(self, guild: Guild) -> str:
        text = self._bot.get_guild_key(guild.id, "fun", "greeting")
        if not text:
            text = "hewwo~"
        return text

    def parse_message(self, msg: Message) -> bool:
        text = msg.content
        greeting = self._say_hello(msg.guild)
        hello_prompts = {
            "hello?": greeting or "hewwo~",
            "nya": "uwu",
            "bitch": "WHAT",
            "you there": "i'm here, frend :3",
        }
        if f"<@{self._bot.user.id}>" not in text:
            return False
        try:
            return next(r for p, r in hello_prompts.items() if p in text)
        except StopIteration:
            return False

    @commands.Cog.listener("on_greetz")
    async def greetz(self, msg: Message, greeting: str) -> None:
        await msg.reply(greeting)

    @commands.command(aliases=["dicks", "nyaa", "honk"])
    async def hewwo(self, ctx: Context) -> None:
        """Say hello!"""
        await ctx.reply(
            await self._say_hello(ctx.guild)
        )


def setup(bot: Bot):
    bot.add_cog(SayHello(bot))
