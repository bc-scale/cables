import logging
from typing import (Optional, Union, List)

from discord.ext import commands
from discord import (
    SlashCommandGroup, Option, option,
    Guild, Member, Role,
    TextChannel, VoiceChannel,
    User, Asset, File, Embed,
    Forbidden, NotFound, InvalidArgument,
)

from cabling_bot.bot import (
    CablesCog as Cog,
    CablesBot as Bot,
    CablesContext as Context,
    CablesAppContext as ApplicationContext,
    CablesAutocompleteContext as AutocompleteContext,
    constants, checks, exceptions,
)
from cabling_bot.util import (
    embeds, interactions,
    transform, queue,
)

logger = logging.getLogger(__name__)

global CHOICES
CHOICES = [
    "anteater",
    "berry",
    "crustacean",
    "option"
]


class TestCmdManager(Cog):
    """Experimental commands not for public consumption (yet?)"""

    def __init__(self, bot: Bot, hidden=True) -> None:
        super().__init__()

        self._bot = bot
        self.log = embeds.EmbedManager(bot)

        global CHOICES

    ##############################
    ## MESSAGE COMMAND EXAMPLES ##
    ##############################

    ## Single command

    # @commands.command("test")
    # async def test(self, ctx: Context):
    #     """Are you feeling lucky?"""
    #     pass

    ## Command group

    # @commands.group(name="test", aliases=[])
    # async def example_group(self, ctx: Context):
    #     """Experimental commands"""
    #     if ctx.invoked_subcommand is None:
    #         await ctx.fail_msg("Invalid command passed")

    # @example_group.command(name="function")
    # async def example_function(self, ctx: Context):
    #     """Are you feeling lucky?"""
    #     pass

    ############################
    ## SLASH COMMAND EXAMPLES ##
    ############################

    ## Single command

    # @commands.slash_command(
    #     name="test",
    #     guild_only=True,
    #     checks=[checks.is_app_owner().predicate]
    # )
    # async def test(self, ctx: ApplicationContext):
    #     """Are you feeling lucky?"""
    #     pass

    ## Command group

    # cmd_group = SlashCommandGroup(
    #     "test",
    #     "Internal slash commands for bot testing purposes.",
    #     guild_only=True,
    #     checks=[checks.is_app_owner().predicate]
    # )

    # @cmd_group.command(name="cmd")
    # @option("option", description="An option", autocomplete=CHOICES)
    # async def test_slash_cmd(
    #     self, ctx: ApplicationContext,
    #     option: str,
    # ):
    #     """Are you feeling lucky?"""
    #     await ctx.respond(f"You selected '{option}!'")
    #     pass

    ######################
    ## CUSTOM TEST CODE ##
    ######################

    ## Your code here

    ## ...

    ## But not past here...

    #####################################
    ## DO NOT COMMIT CODE BELOW HERE!! ##
    #####################################


def setup(bot: Bot):
    bot.add_cog(TestCmdManager(bot))
