import logging
from typing import Optional
from discord import (
    SlashCommandGroup, option,
    TextChannel, Message,
)
from discord.ext import commands

from ..events.cron import CronManager
from ..util import statistics
from ..bot.constants import Emojis
from ..bot import (
    CablesCog as Cog,
    CablesBot as Bot,
    CablesAppContext as ApplicationContext,
    checks
)

logger = logging.getLogger(__name__)


class MsgAggregationCmdMgr(Cog):
    def __init__(self, bot: Bot) -> None:
        super().__init__()
        self._bot = bot

    @property
    def scheduler(self) -> CronManager:
        return self._bot.get_cog("CronManager")

    def _cooldown(self, message: Message) -> Optional[commands.Cooldown]:
        if (
            message.author.id in self._bot.operator
            or message.guild.id == self._bot.dev_guild.id
        ):
            return None
        else:
            return commands.Cooldown(1, 599)

    cmd_group = SlashCommandGroup(
        "stats",
        "Commands for the message statistics module",
        guild_only=True,
    )

    ## MANUAL RUN

    @checks.is_admin()
    @checks.is_feature_enabled("statistics")
    @commands.dynamic_cooldown(_cooldown, commands.BucketType.user)
    @cmd_group.command(name="aggregate")
    @option(
        "days", int,
        description="Period in days (default: 7, max: 31)",
        default=7, min_value=1, max_value=31,
    )
    async def aggregate_cmd(
        self, ctx: ApplicationContext,
        days: Optional[int],
    ) -> None:
        """Count messages for the given period and return a breakdown."""
        await ctx.response.defer()
        await statistics.job_wrapper(
            ctx.bot,
            ctx.interaction, days
        )

    ## SETUP

    @checks.is_admin()
    @cmd_group.command(name="setup")
    @option("channel", TextChannel,
            description="Where to you'd like the statistics to be posted")
    async def setup_cmd(
        self, ctx: ApplicationContext,
        channel: TextChannel
    ) -> None:
        """Setup message statistics reporting for your server."""
        await ctx.response.defer()
        text = ""

        try:
            # check for access to the given channel
            await statistics.perms_check(ctx, ctx.guild, channel)
            text += f"{Emojis.confirm} Check permissions\n"
        except ValueError:
            return

        config = self.init_guild_config(self._bot.db, ctx.guild.id)
        tup = await statistics.configure_guild(self._bot, config, channel)
        (msg, changes) = tup
        if changes:
            text += f"{Emojis.confirm} Update settings\n"

        chid = config.get("statistics", "summary_channel")
        if channel.id != chid:
            text += f"{Emojis.confirm} Create post\n"
            inprogress = f"{Emojis.hourglass} Generate statistics\n"
            pending = f"{Emojis.pause} Enable scheduling"
            reply = await ctx.followup.send(text + inprogress + pending)

            # generate statistics, populating the stub post
            await statistics.job_wrapper(ctx.bot, msg, 7)
            text += f"{Emojis.confirm} Generate statistics\n"

        # enable the cron job for this guild
        task = "message_aggregation"
        if not self.scheduler._db_get_guild_state(ctx.guild.id, task):
            await self.scheduler.toggle_guild_state(ctx.guild.id, task, True)
            text += f"{Emojis.confirm} Enable scheduling"
            changes = True

        skip = f"{Emojis.skip} Nothing to do!"
        if channel.id != chid:
            await reply.edit(text if changes else skip)
        else:
            await ctx.followup.send(text if changes else skip)


def setup(bot: Bot):
    bot.add_cog(MsgAggregationCmdMgr(bot))
