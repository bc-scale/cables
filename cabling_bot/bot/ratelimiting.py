import logging
import asyncio
import inspect
from typing import (Any, Callable, Coroutine, Optional, Union)
from discord import Guild, Member
from .errors import report_guild_error

logger = logging.getLogger(__name__)


async def congestion_detector(
    bot,
    guild: Guild,
    coro: Coroutine,
    timeout: float = 600,
    callback: Any = None,
    err: Optional[str] = "Interaction",
    member: Optional[Member] = None,
) -> bool:
    _, pending = await asyncio.wait(coro, timeout=timeout)
    if not pending:
        return False
    err = f"{err} failed, probably due to Discord service congestion."
    text = ("Discord service congestion is slowing me down! "
            + "Try taking that role again later, please!")
    await report_guild_error(
        bot, None, member,
        guild, err, text
    )
    return True


async def simple_retry(
    method: Union[Coroutine, Callable],
    args: Optional[list[Any]] = [],
    kwargs: Optional[dict] = {},
    attempts: Optional[int] = 2,
) -> Optional[Any]:
    attempt = 0
    while attempt < attempts:
        attempt += 1
        result = None
        try:
            if inspect.isawaitable(method):
                result = await (method)(*args, **kwargs)
            else:
                result = (method)(*args, **kwargs)
        except:  # noqa: E722
            pass
        if not result:
            await asyncio.sleep(0.5)
            continue
        else:
            return result
    return None
