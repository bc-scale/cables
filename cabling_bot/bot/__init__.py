from __future__ import annotations
from typing import TYPE_CHECKING

import inspect
import json
import logging
import os
import toml
import time
import discord

from typing import (
    Any, Dict, List,
    Optional, Union, TypeVar,
    Awaitable, Callable,
)
from discord import (
    ApplicationCommand, Interaction, InteractionMessage, InteractionResponse, Option,
    ClientUser, Permissions, User,
    Guild, Member, Role,
    CategoryChannel, StageChannel, VoiceChannel,
    TextChannel, DMChannel, Thread,
    Message, PartialMessageable, Colour, Embed,
    NotFound, VoiceProtocol, Webhook, WebhookMessage
)

# pycord internal components
import discord.commands
import discord.ext.commands
import discord.utils
from discord.ext.commands import Command
from discord.ext.commands.bot import (AutoShardedBot, Bot)
from discord.ext.commands.view import StringView
from discord.state import ConnectionState
from discord.utils import cached_property

# internal bot components
from cabling_bot.bot import constants
import cabling_bot.bot.config as bot_config
from cabling_bot.bot.config import (
    ConfigKeys, GuildConfig, MemberConfig,
    get_with_env_override,
)

# services
from git.repo import Repo
from ec2_metadata import ec2_metadata
from ..bot.oplog import OpLog
from ..services.postgresql import DatabaseService
from ..services.aws_secrets_manager import SecretsManagerService
from ..services.cryptography import CryptographyService
from ..services.reddit import RedditService
from ..services.hydrus import HydrusService


T = TypeVar("T")
BotT = TypeVar("BotT", bound="Union[Bot, AutoShardedBot, CablesBot]")
InteractionChannel = TypeVar(
    "InteractionChannel",
    bound="Union[VoiceChannel,StageChannel,TextChannel,CategoryChannel,Thread,PartialMessageable]"
)
MISSING: Any = discord.utils.MISSING

logger = logging.getLogger(__name__)


class CablesCog(discord.ext.commands.Cog):
    @staticmethod
    def init_bot_config(db) -> ConfigKeys:
        return bot_config.ConfigKeys(db)

    @staticmethod
    def init_guild_config(db, guild: int) -> Optional[GuildConfig]:
        if type(guild) is not int:
            raise TypeError("init_guild_config: guild must be of type int")
        return bot_config.GuildConfig(db, guild)

    @staticmethod
    def init_member_config(db, guild: int, member: int) -> Optional[MemberConfig]:
        if type(guild) is not int:
            raise TypeError("get_guild_key: guild must be of type int")
        if type(member) is not int:
            raise TypeError("get_guild_key: member must be of type int")
        return bot_config.MemberConfig(db, guild, member)

    def get_guild_key(self, guild: int, module: str, key: str) -> Any:
        if type(guild) is not int:
            raise TypeError("get_guild_key: guild must be of type int")
        if TYPE_CHECKING:
            self._bot: CablesBot
        return self._bot.get_guild_key(guild, module, key)


class CablesContext(discord.ext.commands.Context):
    # we override this to modify the bot type
    # enables intelligent type checking in IDEs to for our methods
    def __init__(
        self,
        *,
        message: Message,
        bot: BotT,
        view: StringView,
        args: List[Any] = MISSING,
        kwargs: Dict[str, Any] = MISSING,
        prefix: Optional[str] = None,
        command: Optional[Command] = None,
        invoked_with: Optional[str] = None,
        invoked_parents: List[str] = MISSING,
        invoked_subcommand: Optional[Command] = None,
        subcommand_passed: Optional[str] = None,
        command_failed: bool = False,
        current_parameter: Optional[inspect.Parameter] = None,
    ):
        self.message: Message = message
        self.bot: BotT = bot
        self.args: List[Any] = args or []
        self.kwargs: Dict[str, Any] = kwargs or {}
        self.prefix: Optional[str] = prefix
        self.command: Optional[Command] = command
        self.view: StringView = view
        self.invoked_with: Optional[str] = invoked_with
        self.invoked_parents: List[str] = invoked_parents or []
        self.invoked_subcommand: Optional[Command] = invoked_subcommand
        self.subcommand_passed: Optional[str] = subcommand_passed
        self.command_failed: bool = command_failed
        self.current_parameter: Optional[inspect.Parameter] = current_parameter
        self._state: ConnectionState = self.message._state

    async def fail_msg(
        self,
        content: str,
        channel: Union[TextChannel, DMChannel] = None,
        reply: Optional[bool] = True,
        delete_after: Optional[float] = 15.0,
    ):
        """Add a failure reaction and send a pretty error message with an embed.

        By default, the message will be deleted after 15 seconds.

        If no channel is provided, the context will be the message target.

        :param ctx: Context
        :param content: Message content of reply to author
        :param channel: A channel object to send the message to
        :param reply: Reply to the message in context
        :param delete_after: Number of seconds to wait in background before deleting our message
        """
        embed = Embed(description=f"{content}", color=Colour.red())
        try:
            if delete_after > 0:
                footer = f"This error message will clear in {int(delete_after)} seconds."
                embed.set_footer(text=footer)
            else:
                delete_after = None

            reference = self.message if reply else None

            if not channel:
                await self.send(embed=embed, reference=reference, delete_after=delete_after)
            else:
                await channel.send(embed=embed, reference=reference, delete_after=delete_after)
        except NotFound:
            await self.author.send(content)

    async def react_success(self):
        """Add a ✅ react to the user's command message"""
        await self.message.add_reaction(constants.Emojis.confirm)

    async def react_fail(self):
        """Add a ❌ react to the user's command message"""
        await self.message.add_reaction(constants.Emojis.deny)

    async def react_disapprovingly(self):
        """Add a 🙅‍♀️ react to the user's command message"""
        await self.message.add_reaction(constants.Emojis.no_gesture)

    async def reply_success(self, content: str):
        embed = Embed(description=content, color=0x3BA55D)
        try:
            await self.reply(embed=embed)
        except NotFound:
            await self.author.send(embed=embed)

    async def reply_disapprovingly(
        self,
        mention: Optional[bool] = False,
        text: Optional[str] = "",
    ) -> None:
        mention = self.author.mention if mention else ""
        await self.message.channel.send(
            f"{mention} {constants.Emojis.no_gesture} {text}",
            delete_after=5
        )

    def get_guild_key(self, guild: int, module: str, key: str) -> Any:
        return self.bot.get_guild_key(guild, module, key)

    @property
    def config(self) -> ConfigKeys:
        return bot_config.ConfigKeys(self.bot.db)

    @property
    def guild_config(self) -> Optional[GuildConfig]:
        if self.guild:
            return bot_config.GuildConfig(self.bot.db, self.guild.id)
        else:
            return None

    @property
    def member_config(self) -> Optional[MemberConfig]:
        if self.guild:
            return bot_config.MemberConfig(self.bot.db, self.guild.id, self.author.id)
        else:
            return None


# TODO: adapt/add fail_msg() to support interactions
class CablesAppContext(discord.commands.ApplicationContext):
    """Represents a Discord application command interaction context.

    This class is not created manually and is instead passed to application
    commands as the first parameter.

    .. versionadded:: 2.0

    Attributes
    -----------
    bot: :class:`.Bot`
        The bot that the command belongs to.
    interaction: :class:`.Interaction`
        The interaction object that invoked the command.
    command: :class:`.ApplicationCommand`
        The command that this context belongs to.
    """

    def __init__(self, bot: BotT, interaction: Interaction):
        self.bot = bot
        self.interaction = interaction

        # below attributes will be set after initialization
        self.command: ApplicationCommand = None  # type: ignore
        self.focused: Option = None  # type: ignore
        self.value: str = None  # type: ignore
        self.options: dict = None  # type: ignore

        self._state: ConnectionState = self.interaction._state

    ## OVERLOADED CUSTOMIZATIONS

    def get_guild_key(self, guild: int, module: str, key: str) -> Any:
        return self.bot.get_guild_key(guild, module, key)

    @property
    def config(self) -> ConfigKeys:
        return bot_config.ConfigKeys(self.bot.db)

    @property
    def guild_config(self) -> Optional[GuildConfig]:
        if self.guild:
            return bot_config.GuildConfig(self.bot.db, self.guild.id)
        else:
            return None

    @property
    def member_config(self) -> Optional[MemberConfig]:
        if self.guild:
            return bot_config.MemberConfig(self.bot.db, self.guild.id, self.author.id)
        else:
            return None

    ## STANDARD SHIT
    # needed for VSCode auto-complete

    @cached_property
    def channel(self) -> Optional[InteractionChannel]:
        """Union[:class:`abc.GuildChannel`, :class:`PartialMessageable`, :class:`Thread`]:
        Returns the channel associated with this context's command. Shorthand for :attr:`.Interaction.channel`."""
        return self.interaction.channel

    @cached_property
    def channel_id(self) -> Optional[int]:
        """:class:`int`: Returns the ID of the channel associated with this context's command.
        Shorthand for :attr:`.Interaction.channel_id`.
        """
        return self.interaction.channel_id

    @cached_property
    def guild(self) -> Optional[Guild]:
        """Optional[:class:`.Guild`]: Returns the guild associated with this context's command.
        Shorthand for :attr:`.Interaction.guild`.
        """
        return self.interaction.guild

    @cached_property
    def guild_id(self) -> Optional[int]:
        """:class:`int`: Returns the ID of the guild associated with this context's command.
        Shorthand for :attr:`.Interaction.guild_id`.
        """
        return self.interaction.guild_id

    @cached_property
    def locale(self) -> Optional[str]:
        """:class:`str`: Returns the locale of the guild associated with this context's command.
        Shorthand for :attr:`.Interaction.locale`.
        """
        return self.interaction.locale

    @cached_property
    def guild_locale(self) -> Optional[str]:
        """:class:`str`: Returns the locale of the guild associated with this context's command.
        Shorthand for :attr:`.Interaction.guild_locale`.
        """
        return self.interaction.guild_locale

    @cached_property
    def app_permissions(self) -> Permissions:
        return self.interaction.app_permissions

    @cached_property
    def me(self) -> Optional[Union[Member, ClientUser]]:
        """Union[:class:`.Member`, :class:`.ClientUser`]:
        Similar to :attr:`.Guild.me` except it may return the :class:`.ClientUser` in private message
        message contexts, or when :meth:`Intents.guilds` is absent.
        """
        return self.interaction.guild.me if self.interaction.guild is not None else self.bot.user

    @cached_property
    def message(self) -> Optional[Message]:
        """Optional[:class:`.Message`]: Returns the message sent with this context's command.
        Shorthand for :attr:`.Interaction.message`, if applicable.
        """
        return self.interaction.message

    @cached_property
    def user(self) -> Optional[Union[Member, User]]:
        """Union[:class:`.Member`, :class:`.User`]: Returns the user that sent this context's command.
        Shorthand for :attr:`.Interaction.user`.
        """
        return self.interaction.user

    author: Optional[Union[Member, User]] = user

    @property
    def voice_client(self) -> Optional[VoiceProtocol]:
        """Optional[:class:`.VoiceProtocol`]: Returns the voice client associated with this context's command.
        Shorthand for :attr:`Interaction.guild.voice_client<~discord.Guild.voice_client>`, if applicable.
        """
        if self.interaction.guild is None:
            return None

        return self.interaction.guild.voice_client

    @cached_property
    def response(self) -> InteractionResponse:
        """:class:`.InteractionResponse`: Returns the response object associated with this context's command.
        Shorthand for :attr:`.Interaction.response`."""
        return self.interaction.response

    @property
    def selected_options(self) -> Optional[List[Dict[str, Any]]]:
        """The options and values that were selected by the user when sending the command.

        Returns
        -------
        Optional[List[Dict[:class:`str`, Any]]]
            A dictionary containing the options and values that were selected by the user when the command
            was processed, if applicable. Returns ``None`` if the command has not yet been invoked,
            or if there are no options defined for that command.
        """
        return self.interaction.data.get("options", None)

    @property
    def unselected_options(self) -> Optional[List[Option]]:
        """The options that were not provided by the user when sending the command.

        Returns
        -------
        Optional[List[:class:`.Option`]]
            A list of Option objects (if any) that were not selected by the user when the command was processed.
            Returns ``None`` if there are no options defined for that command.
        """
        if self.command.options is not None:  # type: ignore
            if self.selected_options:
                return [
                    option
                    for option in self.command.options  # type: ignore
                    if option.to_dict()["name"] not in [opt["name"] for opt in self.selected_options]
                ]
            else:
                return self.command.options  # type: ignore
        return None

    @property
    @discord.utils.copy_doc(InteractionResponse.send_modal)
    def send_modal(self) -> Callable[..., Awaitable[Interaction]]:
        return self.interaction.response.send_modal

    @property
    @discord.utils.copy_doc(InteractionResponse.send_message)
    def send_response(self) -> Callable[..., Awaitable[Interaction]]:
        if not self.interaction.response.is_done():
            return self.interaction.response.send_message
        else:
            raise RuntimeError(
                f"Interaction was already issued a response. Try using {type(self).__name__}.send_followup() instead."
            )

    @property
    @discord.utils.copy_doc(Webhook.send)
    def send_followup(self) -> Callable[..., Awaitable[WebhookMessage]]:
        if self.interaction.response.is_done():
            return self.followup.send
        else:
            raise RuntimeError(
                f"Interaction was not yet issued a response. Try using {type(self).__name__}.respond() first."
            )

    @property
    @discord.utils.copy_doc(InteractionResponse.defer)
    def defer(self) -> Callable[..., Awaitable[None]]:
        return self.interaction.response.defer

    @property
    def followup(self) -> Webhook:
        """:class:`Webhook`: Returns the followup webhook for followup interactions."""
        return self.interaction.followup

    @property
    @discord.utils.copy_doc(Interaction.edit_original_message)
    def edit(self) -> Callable[..., Awaitable[InteractionMessage]]:
        return self.interaction.edit_original_message

    @property
    def cog(self) -> Optional[CablesCog]:
        """Optional[:class:`.Cog`]: Returns the cog associated with this context's command.
        ``None`` if it does not exist.
        """
        if self.command is None:
            return None

        return self.command.cog


class CablesAutocompleteContext(discord.commands.AutocompleteContext):
    """Represents context for a slash command's option autocomplete.

    This class is not created manually and is instead passed to an :class:`.Option`'s autocomplete callback.

    .. versionadded:: 2.0

    Attributes
    -----------
    bot: :class:`.Bot`
        The bot that the command belongs to.
    interaction: :class:`.Interaction`
        The interaction object that invoked the autocomplete.
    command: :class:`.ApplicationCommand`
        The command that this context belongs to.
    focused: :class:`.Option`
        The option the user is currently typing.
    value: :class:`.str`
        The content of the focused option.
    options: Dict[:class:`str`, Any]
        A name to value mapping of the options that the user has selected before this option.
    """

    __slots__ = ("bot", "interaction", "command", "focused", "value", "options")

    def __init__(self, bot: CablesBot, interaction: Interaction):
        if TYPE_CHECKING:
            self._bot: CablesBot
        self.bot = bot
        self.interaction = interaction

        self.command: ApplicationCommand = None  # type: ignore
        self.focused: Option = None  # type: ignore
        self.value: str = None  # type: ignore
        self.options: dict = None  # type: ignore

    @property
    def cog(self) -> Optional[CablesCog]:
        """Optional[:class:`.Cog`]: Returns the cog associated with this context's command.
        ``None`` if it does not exist.
        """
        if self.command is None:
            return None

        return self.command.cog


class CablesBot(discord.ext.commands.Bot):
    def __init__(
        self,
        command_prefix,
        environment,
        app_path,
        config_path,
        help_command=None,
        description: str = None,
        **options
    ):
        super().__init__(
            command_prefix,
            help_command=help_command,
            description=description,
            **options,
        )

        self.api_endpoint = "https://canary.discord.com/api/v10"

        # runtime attributes
        self.environment = environment
        self.app_path = app_path
        self.config_path = config_path
        self.repo = Repo(self.app_path)
        self.dev_guild: Optional[Guild] = None
        self.debug_channel: Optional[TextChannel] = None
        self.operator: Optional[User] = None

        # configuration
        with open(f"{self.app_path}/pyproject.toml", "r") as f:
            _toml = toml.load(f)
            self.project_config = _toml["tool"]["poetry"]
        with open(f"{config_path}/bot.json", "r") as f:
            self.config_dict = json.load(f)
        self._load_config()
        self.config_data: dict[dict[str, Any]] = dict()

        # deployment environment
        self.imds = None
        if self.get_config("aws.running_from_ec2", False):
            self.imds = ec2_metadata
            aws_region = self.imds.region
        else:
            aws_region = os.environ.get("AWS_DEFAULT_REGION", None)

        # temporary file space
        self.tmp_path = "/tmp/cables"
        if not os.path.isdir(self.tmp_path):
            os.mkdir(self.tmp_path)

        # cryptography
        self.secrets = None
        if self.get_config("aws.use_secrets_manager", False):
            self.secrets = SecretsManagerService(region=aws_region).cache
        self.cryptography = CryptographyService(self, self.secrets).controller

        # postgres database client
        self.db = DatabaseService(self).client
        self.config_keys.init_tables()

        # reddit api
        self.reddit = RedditService(self).client

        # hydrus client api
        _endpoint = self.get_config(
            "hydrus.endpoint",
            "http://127.0.0.1:45869/"
        )
        _key = self.get_config("hydrus.api_key", None)
        self.hydrus = None
        if self.get_config("hydrus.enabled", False):
            self.hydrus = HydrusService(_endpoint, _key)

        self.oplog = OpLog(self)

    @property
    def config(self):
        return self.config_dict

    @property
    def config_keys(self):
        return bot_config.ConfigKeys(self.db)

    @staticmethod
    def init_guild_config(db, guild: int) -> Optional[GuildConfig]:
        if type(guild) is not int:
            raise TypeError("init_guild_config: guild must be of type int")
        return bot_config.GuildConfig(db, guild)

    def get_config(self, config_key, default=None) -> Optional[str]:
        maybe_value = get_with_env_override(config_key, self.config_dict)
        if maybe_value:
            self.config_dict[config_key] = maybe_value
        return maybe_value or default

    def get_guild_key(self, guild: int, module: str, key: str) -> Any:
        if type(guild) is not int:
            raise TypeError("get_guild_key: guild must be of type int")
        try:
            return self.config_data[guild][module][key]
        except KeyError:
            config = self.init_guild_config(self.db, guild)
            return config.get(module, key)

    def _load_config(self):
        self.command_prefix = self.get_config("command_prefix", ".")

        _owner_ids = self.config_dict["privileged"].get("bot_admins", [])
        if (self.owner_ids and type(self.owner_ids) is not list):
            _fmt = "'owner_ids' must be a collection, not {0.__class__!r}"
            err = _fmt.format(self.owner_ids)
            raise TypeError(err)
        self.owner_ids = [int(o) for o in _owner_ids]

        logger.info("Loaded configuration from file")
        logger.debug(f"Bot admins: {self.owner_ids}")

        # load project keys into context
        self.issues_uri = self.project_config["urls"]["issues"]
        self.description = (
            self.project_config["description"]
            or "Cables is a bot that isn't all that smart yet."
        )
        self.version, self.repository = "", ""
        pyproject_keys = ["version", "repository"]
        [setattr(self, key, self.project_config[key]) for key in pyproject_keys]

        # get local git repo state
        self.git_commit = self.repo.head.commit
        try:
            self.git_ref = self.repo.head.ref
        except TypeError:
            self.git_ref = 'detached'

    async def update_owners(self) -> User:
        app = await self.application_info()
        if app.owner.id not in self.owner_ids:
            self.owner_ids += [app.owner.id]
        return app.owner

    def get_reach_metrics(self):
        return dict(
            users=len(self.users),
            guilds=len(self.guilds),
            groups=len(self.private_channels)
        )

    def get_privileged_role(self, name: str, guild: Guild) -> Optional[Role]:
        if name not in (
            "staff",
            "global_moderator",
            "channel_moderator",
            "admin",
            "owner_proxy"
        ):
            raise ValueError("Not a valid staff role.")

        role_id = self.get_guild_key(
            guild.id,
            "privileged", f"{name}_role"
        )
        if not role_id:
            return None
        return guild.get_role(role_id)

    def load_extensions(self, extensions: list):
        for ext in extensions:
            result = self.load_extension(f"cabling_bot.{ext}", store=True)
            if result[f"cabling_bot.{ext}"] is not True:
                logger.exception(result[f"cabling_bot.{ext}"])
                time.sleep(2.0)
                continue

    def reload(self):
        """Hot reload all bot extensions/commands and configuration"""
        logger.info("Reloading config...")
        self._load_config()
        logger.info(f"Reloading extensions: {self.extensions}")
        for ext in list(self.extensions.keys()):
            logger.debug(f"Reloading extension {ext}...")
            self.reload_extension(ext)
        logger.info("Finished reloading extensions")

    async def get_context(self, message, *, cls=CablesContext):
        """Required for using custom context"""
        return await super().get_context(message, cls=cls)

    async def get_application_context(self, interaction: Interaction, cls=CablesAppContext):
        """Required for using custom context"""
        return await super().get_application_context(interaction, cls)
