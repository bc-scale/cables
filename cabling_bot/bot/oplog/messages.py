from discord import (
    Message,
    RawBulkMessageDeleteEvent,
    RawMessageDeleteEvent,
    DMChannel,
    TextChannel,
    ForumChannel,
    Thread,
    VoiceChannel,
)


class MessagesComponent:
    def __init__(self, bot):
        self._bot = bot

    def on_message(self, *, messages: list[Message]) -> dict:
        return self._handle_create_delete("CREATE", messages)

    def on_message_edit(self, *, after: Message, before: Message = None) -> dict:
        return self._handle_edit("EDIT", before, after)

    def on_message_delete(self, *, messages: list[Message]) -> dict:
        return self._handle_create_delete("DELETE", messages)

    def on_bulk_message_delete(self, *, messages: list[Message]) -> dict:
        return self._handle_create_delete("DELETE", messages)

    # def on_raw_message_delete(self, *, messages: RawMessageDeleteEvent) -> dict:
    #     return self._handle_create_delete("DELETE", messages)

    # def on_raw_bulk_message_delete(self, *, messages: RawBulkMessageDeleteEvent) -> dict:
    #     return self._handle_create_delete("DELETE", messages)

    def _add_common_attr(
        self,
        log_event: str,
        msg: Message,
        fragment: dict
    ) -> dict:
        if msg.reference and msg.reference.message_id:
            fragment |= dict(reference=msg.reference.message_id)
        elif msg.flags.source_message_deleted:
            fragment |= dict(reference="deleted")

        if msg.webhook_id:
            fragment |= dict(webhook=msg.webhook_id)
        if msg.author.bot:
            fragment |= dict(bot=True)
        if msg.application and getattr(msg.application, id, None):
            fragment |= dict(application=True)

        # TODO: doesnt work despite everything
        #   seemingly matching up
        # if (
        #     log_event.startswith('GUILD')
        #     and (msg.flags.has_thread or msg.flags.value == 18)
        # ):
        #     fragment |= dict(creates_thread=True)

        if msg.flags.ephemeral:
            fragment |= dict(ephemeral=True)

        return fragment

    def _parse_origin(
        self,
        event_type: str,
        msg: Message,
        log: dict
    ) -> tuple[str, dict]:
        log |= dict(message=msg.id)
        log_event = f"UNKNOWN_MESSAGE_{event_type}"
        channel = msg.channel
        if isinstance(channel, DMChannel):
            log_event = f"PRIVATE_MESSAGE_{event_type}"
            log |= dict(channel=msg.channel.id)
        elif type(channel) in (TextChannel, VoiceChannel):
            log_event = f"GUILD_MESSAGE_{event_type}"
            log |= dict(guild=msg.guild.id)
            log |= dict(channel=msg.channel.id)
        elif type(channel) is Thread or isinstance(channel, ForumChannel):
            log_event = f"THREAD_MESSAGE_{event_type}"
            log |= dict(channel=msg.channel.parent_id)
            log |= dict(thread=msg.channel.id)
        return (log_event, log)

    def _get(self, msg: Message) -> Message:
        maybe_msg = self._bot.get_message(msg.id)
        if maybe_msg:
            return maybe_msg
        return msg

    def _handle_create_delete(
        self,
        event_type: str,
        messages: list[Message]
    ) -> dict:
        if event_type == "CREATE":
            msg = messages[0]
            _messages = messages
        else:
            msg = self._get(messages[0])
            messages.pop(0)
            _messages = [msg] + messages

        log_event, items = self._parse_origin(event_type, msg, {})

        msgs = []
        for msg in _messages:
            msg: Message
            if event_type == "DELETE":
                msg = self._get(msg)

            element = dict(message=msg.id)
            element = self._add_common_attr(log_event, msg, element)

            if msg.reference and msg.reference.message_id == msg.channel.id:
                element |= dict(creates_thread=True)
                del element["reference"]

            if msg.embeds:
                element |= dict(embeds=True)
            elif msg.flags.suppress_embeds:
                element |= dict(embeds="suppressed")

            if msg.attachments:
                element |= dict(attachment=True)
            if msg.flags.is_crossposted:
                element |= dict(crossposted=True)
            if msg.is_system():
                element |= dict(system=True)
            if msg.flags.urgent:
                element |= dict(urgent=True)

            msgs += [element]

        if len(msgs) == 1:
            items |= msgs[0]
        elif len(msgs) > 1:
            items |= {"messages": msgs}

        return {log_event: items}

    def _handle_edit(
        self,
        event_type: str,
        before: Message,
        after: Message
    ) -> dict:
        items = dict(channel=after.channel.id)
        log_event, items = self._parse_origin(event_type, after, items)

        items: dict = self._add_common_attr(log_event, before, items)

        if after.flags.suppress_embeds:
            items |= dict(embeds="suppressed")

        if before.attachments and not after.attachments:
            items |= dict(attachments="removed")
        elif before.attachments and before.attachments != after.attachments:
            items |= dict(attachments="changed")

        return {log_event: items}
